FILE_VALA=main.vala
FLAGS=--pkg=gtk+-3.0
OBJECTS=${FILE_VALA:.vala=.vala.o}
NAME=a.out

all: $(NAME)

$(NAME):$(OBJECTS)
	clang $(OBJECTS) -o $(NAME) `pkg-config --libs gtk+-3.0`

$(OBJECTS):
	valac $(FLAGS) --vapidir ./libvapi -c $(FILE_VALA)

clean:
	rm -f $(OBJECTS)
	rm -f $(FILE_C:.c=.o)
mrproper: clean
	rm -f $(NAME)
